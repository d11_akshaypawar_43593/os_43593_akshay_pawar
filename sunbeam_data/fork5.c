#include <stdio.h>
#include <unistd.h>

/*
when parent is terminated before child process, then the child process is called as "orphan" process.
ownership/parenting of such process is taken by init/systemd (pid=1) process.
*/

int main()
{
	int ret, i;
	ret = fork();
	if(ret == 0)
	{	// child process
		for(i=0; i<30; i++) 
		{
			printf("child: %d\n", i);
			sleep(1);
		}
	}
	else
	{	// parent process
		for(i=0; i<15; i++) 
		{
			printf("parent: %d\n", i);
			sleep(1);
		}	
	}
	return 0;
}


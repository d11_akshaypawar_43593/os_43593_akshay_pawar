#include <stdio.h>
#include <unistd.h>

/*
parent and child processes are executing concurrenly.
*/

int main()
{
	int ret, i;
	ret = fork();
	if(ret == 0)
	{	// child process
		for(i=0; i<30; i++) 
		{
			printf("child: %d\n", i);
			sleep(1);
		}
	}
	else
	{	// parent process
		for(i=0; i<30; i++) 
		{
			printf("parent: %d\n", i);
			sleep(1);
		}	
	}
	return 0;
}


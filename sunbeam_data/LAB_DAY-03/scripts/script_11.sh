#!/bin/bash

# script to write a function to print a table

function table( )
{
	echo "table of $1 is: "
	for i in {1..10..1}
	do
		res=`expr $1 \* $i`
		echo "$res"
	done
}

clear
echo "script started !!!"

echo -n "enter number	: "
read num

#syntax-1 to give call to the function
#table $num

#syntax-2
tab=$( table $num )
echo "tab is: "
echo "$tab"


echo "script exited !!!"

exit












#!/bin/bash

# script to do addition of two positional params


clear

echo "no. of positional params = $#"

if [ $# -ne 2 ]	# if no. of pos params are not exactly 2
then
	echo "invalid no. of pos params to the script : $0"
	exit # script gets exited
fi

sum=`expr $1 + $2`
echo "sum = $sum"

exit



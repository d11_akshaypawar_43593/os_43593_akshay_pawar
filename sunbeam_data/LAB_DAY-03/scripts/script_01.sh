#!/bin/bash

# above line is called as "shebang line" : which starts with pound sign "#" followed by ! 
# is an abs path of shell program which will going to execute script.
# to write shebang line shebang line is optional if current shell is "bash", but if current
# shell is not bash then to mention shabang is mandatory.

# in shell script programming langauge comment starts with "#"

clear

echo -n "today's date is: "

date +"%d/%m/%Y"

echo "cal of current month is: "
cal

# below exit is used to exit script and not to exit terminal
exit


#!/bin/bash

# script to add all positional params

# clear

sum=0

# 10 20 30 40 50
for num in $*
do
	sum=`expr $sum + $num`	
done

echo "sum of all positional params is	: $sum"

exit


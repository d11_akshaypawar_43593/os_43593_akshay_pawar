#!/bin/bash

# script to write a function to do addition of 2 numbers

function addition( )
{
	res=`expr $1 + $2`
	echo "$res"
}


clear
echo "script started !!!"

echo -n "enter n1: "
read n1

echo -n "enter n2: "
read n2

#syntax-1: in this syntax of function calling echo command is used for printing purpose
#addition $n1 $n2

#syntax-2: in this syntax of function calling echo command is used for storing value to return
#from the function
sum=$( addition $n1 $n2 )
echo "sum is : $sum"



echo "script exited !!!"
exit



#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

int cnt = 0;

void* thread1_func(void *param)
{	// thread 1
	int i;
	for(i=1; i<=3000; i++)
	{
		cnt++;
		printf("thread1: %d\n", cnt);
		//sleep(1);
	}
	return 0;
}

void* thread2_func(void *param)
{	// thread 2
	int i;
	for(i=1; i<=3000; i++)
	{
		cnt--;
		printf("thread2: %d\n", cnt);
		//sleep(1);
	}
	return 0;
}

int main()
{	// main thread
	pthread_t t1, t2;
	int i;
	pthread_create(&t1, NULL, thread1_func, NULL);
	pthread_create(&t2, NULL, thread2_func, NULL);
	printf("main thread will wait for given thread...\n");
	pthread_join(t1, NULL);
	pthread_join(t2, NULL);
	printf("final count = %d\n", cnt);
	return 0;
}



# Operating Systems - Linux

## Learning OS
* step 1: End user - Linux commands
* step 2: Administrator (Super-user) - Linux shell scripts
* step 3: Programmer - Linux System calls
* step 4: Designer - UNIX/Linux/OS Internals

## Reference
* Book 1: Operating System Concepts -- Galvin
* Book 2: Beginning Linux Programming -- Mathews
* Slides

## Evaluation
* Theory: MCQ based - CCEE
* Lab: 40 marks - Linux commands & shell scripts - 13-Feb
* Internal: 20 marks - Quiz

## Schedule
* Mon-Sat Lecture: 8:00 am to 1:00 pm - Nilesh
* Mon-Sat Lab: 2:30 pm to 6:30 pm - Sachin

# Operating System

## Introduction

## OS Functions














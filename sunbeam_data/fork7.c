#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

/*
wait() syscall:
1. pause execution of parent until one of its child is terminated.
2. read the exit status of the child from its pcb.
3. release the pcb of the child.

unix - wait()
linux - wait(), waitpid()
*/

int main()
{
	int ret, i, s;
	ret = fork();
	if(ret == 0)
	{	// child process
		for(i=0; i<15; i++) 
		{
			printf("child: %d\n", i);
			sleep(1);
		}
		_exit(3);
	}
	else
	{	// parent process
		for(i=0; i<30; i++) 
		{
			printf("parent: %d\n", i);
			sleep(1);
			if(i == 15) // 10
			{
				wait(&s);
				printf("child exit: %d\n", WEXITSTATUS(s));
			}
		}	
	}
	return 0;
}


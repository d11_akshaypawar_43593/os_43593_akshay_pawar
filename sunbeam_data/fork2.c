#include <stdio.h>
#include <unistd.h>

// printf() called 8 times.
// refer diagram.

int main()
{
	fork();
	fork();
	fork();
	printf("hello world!\n");
	return 0;
}


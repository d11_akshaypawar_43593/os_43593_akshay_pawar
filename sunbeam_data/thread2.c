#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

/*
process is a container that holds resources required for execution of the process.
threads are unit of execution/scheduling.

CPU scheduler assign time to thread.
*/

void* thread1_func(void *param)
{	// thread 1
	int i;
	for(i=1; i<=30; i++)
	{
		printf("thread1: %d\n", i);
		sleep(1);
	}
	return 0;
}

void* thread2_func(void *param)
{	// thread 2
	int i;
	for(i=1; i<=30; i++)
	{
		printf("thread2: %d\n", i);
		sleep(1);
	}
	return 0;
}

int main()
{	// main thread
	pthread_t t1, t2;
	int i;
	pthread_create(&t1, NULL, thread1_func, NULL);
	pthread_create(&t2, NULL, thread2_func, NULL);
	for(i=1; i<=15; i++)
	{
		printf("main: %d\n", i);
		sleep(1);
	}
	return 0;
}

/*
when main thread is terminated, the process get terminated.

when process is terminated, all threads in that process are terminated/destroyed.
*/


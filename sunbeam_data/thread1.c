#include <stdio.h>
#include <unistd.h>
#include <pthread.h>

/*
thread is light-weight process.

for each thread, thread control block is created and stack section is created.
rest of the sections are shared with parent process.
*/

void* thread1_func(void *param)
{	// thread 1
	int i;
	for(i=1; i<=30; i++)
	{
		printf("thread1: %d\n", i);
		sleep(1);
	}
	return 0;
}

void* thread2_func(void *param)
{	// thread 2
	int i;
	for(i=1; i<=30; i++)
	{
		printf("thread2: %d\n", i);
		sleep(1);
	}
	return 0;
}

int main()
{	// main thread
	pthread_t t1, t2;
	int i;
	pthread_create(&t1, NULL, thread1_func, NULL);
	pthread_create(&t2, NULL, thread2_func, NULL);
	for(i=1; i<=30; i++)
	{
		printf("main: %d\n", i);
		sleep(1);
	}
	return 0;
}

/*
ret = pthread_create(&thread_id, &thread_attributes, thread_function, thread_function_param);
	arg1: out param -- get thread id.
	arg2: thread attributes -- NULL means default attributes
		- stack size
		- priority
		- scheduling policy
	arg3: function to be executed by the thread.
	arg4: argument to the thread function.
	return: 0 on success.
*/

/*
terminal> gcc thread1.c -o thread.out -lpthread
terminal> ./thread.out
*/


#include<stdio.h>
#include<unistd.h>
/*
process is terminated when main returns/exit() is called.
_exit() syscall release the resources of the process.
write its exit value into its pcb (so that its parent can read it later).
it also send signal (SIGCHLD) to the parent, indicating death of the process.

when child is terminated before parent and parent is not reading exit status of its child,
child process resources are released, but its exit status is still maintained in its pcb.
this state of child is called "zombie state".
*/
int main(){
	int ret, i;
	ret = fork();

	if(ret == 0){
		for(i = 0; i < 15; i++){
			printf("child : %d\n", i);
			sleep(1);
		}
	}
	else{
		for(i = 0; i < 30; i++){
			printf("parent : %d\n", i);
			sleep(1);
		}
	}
	return 0;
}


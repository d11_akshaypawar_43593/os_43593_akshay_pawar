#include<stdio.h>
#include<unistd.h>

/*
fork() creates a new process by duplicating calling process.
the new process is called as child process and calling process is called as parent process.
parent and child both have separate memory space.
child process have its own unique pid.

fork() returns 0 to the child process.
fork() returns pid of child to the parent process.
*/

int main(){
	int ret;
	printf("Program started\n");
	ret = fork();
	printf("fork returned: %d\n", ret);
	if(ret == 0){
		printf("child: pid =  %d\n",getpid());
		printf("child:parent:  ppid =  %d\n",getppid());
	}
	else{
		printf("parent: pid =  %d\n",getpid());
		printf("parent:parent:  ppid =  %d\n",getppid());
	}
	printf("program completed");
	getchar();
	return 0;
	
}

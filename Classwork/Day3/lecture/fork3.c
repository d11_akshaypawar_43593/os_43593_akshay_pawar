#include<stdio.h>
#include<unistd.h>

/*
child process is created when some task is to be done concurrently.
child process run concurrently with parent process (depending on scheduler).

using if-else can assign separate task for parent and child.
if block -- child process
else block -- child process
*/

int main(){
	int ret, i;
	ret = fork();

	if(ret == 0){
		for(i = 0; i < 3000; i++){
			printf("child : %d\n", i);
		}
	}
	else{
		for(i = 0; i < 3000; i++){
			printf("parent : %d\n", i);
		}
	}
	return 0;
}


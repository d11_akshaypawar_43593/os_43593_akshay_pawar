#!/bin/bash

#script to write a function to print a table

function table(){

	echo "table of $1 is : "

	for i in {1..10..1}
	do
		res=`expr $1 \* $i`
		echo "$res"
	done
}


clear

echo "execution started"

echo "enter no to print table"
read num

# calling function
# 1st syntex
# table $num

# 2nd syntex
tab=$( table $num )
echo "tab is"
echo $tab

echo "script exited"


exit

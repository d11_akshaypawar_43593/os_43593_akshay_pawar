#!/bin/bash

#to print name of execuatable files in current directory

clear

echo "list of executable files in cur dir are"

for filepath in `ls`
do
	if [ -e $filepath -a -x $filepath ]
	then
		echo $filepath
	fi
done

exit

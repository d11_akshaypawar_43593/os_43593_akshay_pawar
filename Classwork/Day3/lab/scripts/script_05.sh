#!/bin/bash

#script to check leap year
#if(y % 4 == 0 && y % 100 != 0 || y % 400 == 0)

clear

echo -n "Enter the year : "
read yr

if [ `expr $yr % 4` -eq 0 -a `expr $yr % 100` -ne 0 -o `expr $yr % 400` -eq 0 ]
then
	echo "leap year"
else
	echo "not leap year"
fi

exit

#!/bin/bash

#above line is called as shebang line starts with pound sign "#" and followed by !
#if current shell is bash then shebang line is optional 
#if current shell is not bash then writing shebang is mandotary

clear

echo "todays date is: "

date +"%d/%m/%y"

echo "calander of this month is :"

cal

# to exit script not the terminal
exit

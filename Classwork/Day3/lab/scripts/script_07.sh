#!/bin/bash

#script to print the table of given num

clear 

echo "enter number : "
read num

i=1
echo "table of $num is :"

while [ $i -le 10 ]
do
 	res=`expr $num \* $i`
	echo "$res"
	i=`expr $i + 1`
done

i=1
echo "until loop table : "

until [ $i -gt 10 ]
do
 	res=`expr $num \* $i`
	echo "$res"
	i=`expr $i + 1`
done

echo "for loop table : "

for i in {1..10..1}
do
 	res=`expr $num \* $i`
	echo "$res"
done
exit

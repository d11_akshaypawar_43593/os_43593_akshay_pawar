#!/bin/bash

# script to add 2 pos parameters

clear

echo "no. of pos parameters = $#"

if [ $# -ne 2 ]
then 
	echo "invalid numbers of pos parameters to scripts = $0"
	exit
fi

sum=`expr $1 + $2`
echo "sum = $sum"

exit

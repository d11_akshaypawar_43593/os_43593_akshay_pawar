#!/bin/bash

#accept filepath from user check wether valid or invalid if invalid display
#message invalid filepath if valid check filepath is regular file display
#contents if directory display contents
#otherwise display msg filepath is nither regular not directory

clear

echo -n "enter filepath : "
read fp

if [ -e $fp ] #valid fp
then
	if [ -f $fp ]
	then
		echo "contents of regular file : "
		cat $fp
	elif [ -d $fp ]
	then
		echo "contents of diroctory file : "
		ls $fp
	else
		echo "$fp is nither directory or regular file"
	fi
else
	echo "filepath is not valid"
fi
